# Tài liệu chung về Quy Tắc và Hướng Dẫn cộng tác kỹ thuật

## I. Đối với mục tiêu cộng tác kết nối hệ thống công nghệ song phương

### 1. Xác định mục tiêu / kết quả mà 2 hệ thống đạt được sau khi kết nối

Dựa trên thoả thuận hợp tác song phương và các tài liệu kỹ thuật liên quan. Xác định:

- Sự thay đổi của hệ thống
    - Tính năng bổ sung
    - Tính năng bị thay đổi
    - Tính năng bị huỷ bỏ
- Sự ảnh hưởng đối với các hệ thống ngoại vi (các hệ thống đối tác đã kết nối trước đó)
    - Các dịch vụ không bị ảnh hưởng (*)
    - Các dịch vụ bị ảnh hưởng nhưng có thể xử lý ở phía Sơn Phát (sửa mã nguồn, etc..)
    - Các dịch vụ bị ảnh hưởng nhưng có thể xử lý ở phía đối tác còn lại
    - Các dịch vụ bị ảnh hưởng và không thể khắc phục (có thời hạn hoặc vĩnh viễn)
- Sự ảnh hưởng đối với khách hàng
    - Các khách hàng hiện tại đang sử dụng dịch vụ có bị ảnh hưởng không?
    - Các khách hàng mới có phải bổ sung thêm thông tin gì không?
    - Có tăng phạm vi khách hàng tiềm năng hay không?
- Sự ảnh hưởng đối với đội ngũ kinh doanh
    - Có thay đổi công cụ làm việc?
    - Có thay đổi tài liệu công việc?
    - Có thay đổi nội dung truyền tải đến khách hàng?
- Sự ảnh hưởng đối với đội ngũ phát triển
    - Quá trình chuẩn bị kỹ thuật
    - Nỗ lực thực hiện
    - Thay đổi nền tảng công nghệ hoặc phương pháp thực hiện

### 2. Xác định vai trò của từng hệ thống

```
Xác định các quy trình mẫu, các sự kiện kích hoạt, các hành vi ảnh hưởng đến hệ thống.
Xác định nền tảng công nghệ, phương pháp hoạt động, giao thức triển khai.
``` 

Từ đó đánh giá, thống nhất vai trò CHỦ ĐỘNG / BỊ ĐỘNG của mỗi hệ thống trên toàn quy trình hoặc từng pha của quy trình.

Xác định mô hình liên lạc (kết nối) giữa hai hệ thống: Song công, Bán song công, Đơn hướng, Đa hướng
Xác định công nghệ liên lạc (kết nối): HTTP (SOAP,API,REST,WebHook,...), Socket (TCP,UDP,...), WebSocket, gRPC,...

### 3. Xác định tải trọng dữ liệu

```
Xác định định dạng dữ liệu, 
```

### 4. Xác định tài liệu mô tả kỹ thuật 

```
Nếu là API, sử dụng OAS3
Nếu là SOAP, sử dụng ...
Nếu là gRPC, sử dụng proto3
```

### 
### 